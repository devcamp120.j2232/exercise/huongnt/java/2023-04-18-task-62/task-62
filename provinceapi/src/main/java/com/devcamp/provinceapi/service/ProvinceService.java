package com.devcamp.provinceapi.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.provinceapi.models.CDistrict;
import com.devcamp.provinceapi.models.CProvince;
import com.devcamp.provinceapi.repository.IProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    IProvinceRepository pProvinceRepository;
    public ArrayList<CProvince> getAllProvince(){
        ArrayList<CProvince> provinceList = new ArrayList<>();
        pProvinceRepository.findAll().forEach(provinceList::add);
        return provinceList;
        
    }
    

    //get district by provinceid
    public Set<CDistrict> getDistrictByProvinceId(int id){
        CProvince vProvince = pProvinceRepository.findById(id);
        if ( vProvince != null){
            return vProvince.getDistricts();
        }
        else return null;
    }
}

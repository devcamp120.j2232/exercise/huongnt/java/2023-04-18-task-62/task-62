package com.devcamp.provinceapi.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.CDistrict;
import com.devcamp.provinceapi.models.CProvince;
import com.devcamp.provinceapi.service.ProvinceService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ProvinceController {
    @Autowired
    private ProvinceService provinceService;
    @GetMapping("/provinces")
    public ResponseEntity<List<CProvince>> getAllProvincesApi(
        @RequestParam(value = "page", defaultValue = "1") String page,
        @RequestParam(value = "size", defaultValue = "5") String size){
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            return new ResponseEntity<>(provinceService.getAllProvince(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    //get district by Province_id
    @GetMapping("/district_by_province")
    public ResponseEntity<Set<CDistrict>> getDistrictByProvinceIdApi(@RequestParam(value = "provinceId") int id){
        try {
            Set<CDistrict> provinceDistrict = provinceService.getDistrictByProvinceId(id);
            if (provinceDistrict != null ){
                return new ResponseEntity<>(provinceDistrict, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
